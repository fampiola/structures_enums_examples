#include <iostream>
#include "Structs.cpp"
using namespace std;

void PrintColorValues(Color);
void PrintColorInformation(ColorStructure);

int main()
{
    ColorStructure colorStr;
    colorStr.mColor = red;
    colorStr.mName = "Red Color";
    PrintColorInformation(colorStr);

    for (int i = red; i <= blue ; i++)
    {
        Color color = static_cast<Color>(i);
        PrintColorValues(color);
        cout << i << endl;
    }
}

void PrintColorValues(Color _colorEnum) 
{
    switch (_colorEnum)
    {
        case red:
            cout << "Red" << endl;
            break;
        case yellow:
            cout << "Yellow" << endl;
            break;
        case green:
            cout << "Green" << endl;
            break;
        case blue:
            cout << "Blue" << endl;
            break;
        default:
            break;
    }
}

void PrintColorInformation(ColorStructure _obj) 
{
    cout << "Color Enum value: " << _obj.mColor << endl;
    cout << "Color Name:" << _obj.mName << endl;
}